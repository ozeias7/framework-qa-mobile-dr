*** Settings ***
Documentation           Todas os BDD's convertidos em keywords de login estarão presentes neste arquivo.

Resource                ../../config/package.robot

*** Keywords ***
Dado que eu abra o aplicativo
    Wait Until Page Contains                ${HOME.BTN_RECUSAR}
    Click Text                              ${HOME.BTN_RECUSAR}
    Wait Until Element Is Visible           ${HOME.BTN_ENTRAR_HOME}
    Click Element                           ${HOME.BTN_ENTRAR_HOME}

Quando inserir as credenciais de acesso
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.EMAIL}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}    
    
    Wait Until Element Is Visible           ${LOGIN_SENHA.INPUT_SENHA}         
    Input Text                              ${LOGIN_SENHA.INPUT_SENHA}    ${AUTH.PASSWORD}
    
    Wait Until Element Is Visible           ${LOGIN_SENHA.BTN_ENTRAR}
    Click Element                           ${LOGIN_SENHA.BTN_ENTRAR}

Quando inserir as credenciais de email invalido
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${AUTH.WRONG_EMAIL}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}

Quando inserir as credenciais de email em branco
    Wait Until Element Is Visible           ${LOGIN.CAMP_EMAIL_TELEFONE}                
    Input Text                              ${LOGIN.CAMP_EMAIL_TELEFONE}    ${EMPTY}
    Wait Until Element Is Visible           ${LOGIN.BTN_CONTINUAR}
    Click Element                           ${LOGIN.BTN_CONTINUAR}


# Então o login deve ser concluído com sucesso
#     Wait Until Element Is Visible           ${HOME.LABEL_TITLE}
#     Wait Until Element Is Visible           ${HOME.BTN_PERFIL}
#     Wait Until Element Is Visible           ${HOME.LABEL_SALDO}
#     Wait Until Page Contains                ${HOME.BTN_EXTRATO}
#     Wait Until Page Contains                ${MSG.HOME.MSG_USER}