*** Settings ***
Documentation           Arquivo responsável por armazenar todas as variáveis e identificadores dos componentes da tela Home
...                     do aplicativo do banco.

*** Variables ***
&{HOME}
...         BTN_RECUSAR=RECUSAR
...         BTN_ENTRAR_HOME=//*[contains(@text, 'Entrar')]
