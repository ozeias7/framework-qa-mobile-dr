*** Settings ***
Documentation           Setup responsável por importar todas as dependências do framework.
########################################################################
##                                                                    ##
##                    Libraries de todo o projeto                     ##
##                                                                    ##
## #####################################################################
## CRIADO POR: Ozeias da Silva									  ##
## DATA: 19/01/2021													  ##
## ÁREA: QUALITY ASSURANCE                                            ##
## #####################################################################

Library                 AppiumLibrary
Library                 FakerLibrary        locale=pt_BR
Library                 DebugLibrary
Library                 OperatingSystem
Library                 Process
Library                 String
Library                 Collections
Library                 pabot.PabotLib

########################################################
#                    Keywords Mobile                   #
########################################################

Resource                ../auto/keywords/kws_login.robot

########################################################
#                    Pages Mobile                      #
########################################################

Resource                ../auto/elements/android/elements_home.robot
Resource                ../auto/elements/android/elements_login_email_telefone.robot
Resource                ../auto/elements/android/elements_login_senha.robot
########################################################
#                     Data Mobile                      #
########################################################
Variables               hooks.yaml
Variables               ../auto/data/login/login_data.yaml
Variables               ../auto/data/login/messages.yaml

########################################################
#                     Hooks /Utils                     #
########################################################

Resource                hooks.robot
Resource                ../utils/common.robot
