*** Settings ***
Documentation           Os processos de setUp e tearDown estarão presentes aqui neste arquivo.
...                     A ideia é termos segregado a inicialização de cada tecnologia.

Resource                package.robot

*** Keywords ***
Open Session Android Local
    Set Appium Timeout          ${CONFIG.MOBILE.DEFAULT.APPIUM_TIMEOUT}
    Open Application            http://localhost:4723/wd/hub
    ...                         automationName=UiAutomator2
    ...                         platformName=${CONFIG.MOBILE.ANDROID.PLATFORM_NAME}
    ...                         deviceName=${CONFIG.MOBILE.ANDROID.DEVICE_NAME}
    ...                         app=${CONFIG.MOBILE.ANDROID.APP_PATH}
    ...                         autoGrantPermissions=true

Open Session Android BrowserStack
    Set Appium Timeout          ${CONFIG.MOBILE.DEFAULT.APPIUM_TIMEOUT}
    Open Application            http://hub-cloud.browserstack.com/wd/hub
    ...                         browserstack.user=${CONFIG.MOBILE.DEVICE_FARM.BSTACK_USER}
    ...                         browserstack.key=${CONFIG.MOBILE.DEVICE_FARM.BSTACK_KEY}
    ...                         app=${CONFIG.MOBILE.DEVICE_FARM.APP_URL}
    ...                         device=${CONFIG.MOBILE.DEVICE_FARM.DEVICE_NAME}
    ...                         os_version=${CONFIG.MOBILE.DEVICE_FARM.OS_VERSION}
    ...                         project=Dr Consulta Aplicativo - Mobile Android
    ...                         build=[ QA - ${CONFIG.MOBILE.DEVICE_FARM.PLATFORM_NAME} ] - Release 2.05353.1 (2)
    ...                         name=${TEST_NAME}
    ...                         autoGrantPermissions=true

Open Session Ios Local
    Set Appium Timeout          ${CONFIG.MOBILE.DEFAULT.APPIUM_TIMEOUT}
    Open Application            http://localhost:4723/wd/hub
    ...                         automationName=XCUITest
    ...                         platformName=${CONFIG.MOBILE.IOS.PLATFORM_NAME}
    ...                         deviceName=${CONFIG.MOBILE.IOS.DEVICE_NAME}
    ...                         udid=${CONFIG.MOBILE.IOS.UDID}
    ...                         app=${CONFIG.MOBILE.IOS.APP_PATH}

# Open Session Ios BrowserStack
#     Set Appium Timeout          ${CONFIG.MOBILE.DEFAULT.APPIUM_TIMEOUT}
#     Open Application            http://hub-cloud.browserstack.com/wd/hub
#     ...                         browserstack.user=%{BROWSERSTACK_USERNAME}
#     ...                         browserstack.key=%{BROWSERSTACK_ACCESS_KEY}
#     ...                         app=%{BROWSERSTACK_APP_ID}
#     ...                         device=${CONFIG.MOBILE.IOS.DEVICE_NAME}
#     ...                         os_version=${CONFIG.MOBILE.IOS.OS_VERSION}
#     ...                         project=Bari Aplicativo - Mobile iOS
#     ...                         build=[ QA - ${PLATFORM}]
#     ...                         name=${TEST_NAME}

Close Session
    Capture Page Screenshot
    Close Application
