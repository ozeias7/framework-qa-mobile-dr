*** Settings ***
Documentation           Todas as keywords que poderão ser utilizadas de forma híbrida estarão presentes aqui.

Resource                ../config/package.robot

*** Keywords ***
Input Password Brasil
    #Wait Until Element Is Visible    ${LOGIN.BTN_ESQUECI_SENHA}
    Sleep                            5

    Click Element                    ${LOGIN.INPUT_PASSWORD}
    Run Process                      adb shell input text ${AUTH.BRASIL.PASSWORD}   shell=True

Input Password Argentina
    #Wait Until Element Is Visible    ${LOGIN.BTN_ESQUECI_SENHA}

    Sleep                            5

    Click Element                    ${LOGIN.INPUT_PASSWORD}

    Click Element                    ${LOGIN.INPUT_PASSWORD}
    Run Process                      adb shell input text ${AUTH.ARGENTINA.PASSWORD}   shell=True