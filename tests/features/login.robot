*** Settings ***
Documentation           Como usuário do aplicativo DR Consulta, quero abrir o aplicativo e informando minhas credenciais
...                     devo realizar o login com sucesso.

Resource                ../../src/config/package.robot

Test Setup              Open Session ${CONFIG.MOBILE.ANDROID.PLATFORM_NAME} ${CONFIG.MOBILE.DEFAULT.AMBIENTE}
Test Teardown           Close Session

*** Test Cases ***
Cenario 1: Realizar login no aplicativo Dr Consulta com sucesso
    [Tags]          LOGIN-1
    Dado que eu abra o aplicativo
    Quando inserir as credenciais de acesso
    # Então o login deve ser concluído com sucesso

Cenario 2: Realizar login no aplicativo Dr Consulta passando email invalido
    [Tags]          LOGIN-2
    Dado que eu abra o aplicativo
    Quando inserir as credenciais de email invalido

Cenario 3: Realizar login no aplicativo Dr Consulta passando email em branco
    [Tags]          LOGIN-3
    Dado que eu abra o aplicativo
    Quando inserir as credenciais de email em branco




    